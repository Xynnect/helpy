﻿helpy2mini:
    Gui Example_with_tabs_:Destroy

    ; SplashTextOn,,,Loading,

    global var_to_check_if_WhiteLineNumberMain2_exists = 0
    global var_to_check_if_WhiteLineNumberMain3_exists = 0
    global var_to_check_if_WhiteLineNumberMain4_exists = 0
    global var_to_check_if_WhiteLineNumberMain5_exists = 0
    global var_to_check_if_WhiteLineNumberMain6_exists = 0
    global var_to_check_if_WhiteLineNumberMain7_exists = 0
    global var_to_check_if_WhiteLineNumberMain8_exists = 0

    CoordMode, Mouse, Screen
    MouseGetPos, xpos, ypos

    WinGet, current_ID, ID, A
    previous_ID := current_ID

    k_Position = x+0 0

    ; +HwndGuiHwnd

    Gui, Example_with_tabs_:+LastFound +MinSize600x400 
    Gui, Example_with_tabs_:Margin, 10, 10
    Gui, Example_with_tabs_:Color, 000000

    MenuHoverLocation                = %A_ScriptDir%\AHKassociatedFiles\ScriptDividedByParts_version_for_direct_calling\Directory_For_ExampleGuiWithTabs-8Tabs\images\button-menu-hover.png
    MenuPressedLocation              = %A_ScriptDir%\AHKassociatedFiles\ScriptDividedByParts_version_for_direct_calling\Directory_For_ExampleGuiWithTabs-8Tabs\images\button-menu-pressed.png
    MenuSelectLocation               = %A_ScriptDir%\AHKassociatedFiles\ScriptDividedByParts_version_for_direct_calling\Directory_For_ExampleGuiWithTabs-8Tabs\images\button-menusel.png
    MenuSelectPurpleLocation         = %A_ScriptDir%\AHKassociatedFiles\ScriptDividedByParts_version_for_direct_calling\Directory_For_ExampleGuiWithTabs-8Tabs\images\button-menusel-purple.png
    MenuAlternativeColourLocation    = %A_ScriptDir%\AHKassociatedFiles\ScriptDividedByParts_version_for_direct_calling\Directory_For_ExampleGuiWithTabs-8Tabs\images\button-menu-hover-alternative-colour.png

    Gui, Example_with_tabs_:Add, Picture, vMenuHover HwndhMenuHover Hidden1,                        %MenuHoverLocation%
    Gui, Example_with_tabs_:Add, Picture, vMenuPressed HwndhMenuPressed Hidden1,                    %MenuPressedLocation%
    Gui, Example_with_tabs_:Add, Picture, vMenuSelect HwndhMenuSelect,                              %MenuSelectLocation%
    Gui, Example_with_tabs_:Add, Picture, vMenuSelectPurple HwndhMenuSelectPurple,                  %MenuSelectPurpleLocation%
    Gui, Example_with_tabs_:Add, Picture, vMenuAlternativeColour HwndhMenuAlternativeColour,        %MenuAlternativeColourLocation%
    Gui, Example_with_tabs_:Add, Picture, vBtnBackN HwndhBtnBackN Hidden1,                          %BtnBackNLocation%
    Gui, Example_with_tabs_:Add, Picture, vBtnBackH HwndhBtnBackH Hidden1,                          %BtnBackHLocation%
    Gui, Example_with_tabs_:Add, Picture, vBtnBackP HwndhBtnBackP Hidden1,                          %BtnBackPLocation%
    ; Gui, Example_with_tabs_:Add, Picture, vBtnCloseN HwndhBtnCloseN,                                %BtnCloseNLocation%
    Gui, Example_with_tabs_:Add, Picture, vBtnCloseH HwndhBtnCloseH Hidden1,                        %BtnCloseHLocation%
    Gui, Example_with_tabs_:Add, Picture, vBtnCloseP HwndhBtnCloseP Hidden1,                        %BtnClosePLocation%
    ; Gui, Example_with_tabs_:Add, Picture, vBtnMaxN HwndhBtnMaxN,                                    %BtnMaxNLocation%
    Gui, Example_with_tabs_:Add, Picture, vBtnMaxH HwndhBtnMaxH Hidden1,                            %BtnMaxHLocation%
    Gui, Example_with_tabs_:Add, Picture, vBtnMaxP HwndhBtnMaxP Hidden1,                            %BtnMaxPLocation%
    Gui, Example_with_tabs_:Add, Picture, vBtnRestoreN HwndhBtnRestoreN Hidden1,                    %BtnRestoreNLocation%
    Gui, Example_with_tabs_:Add, Picture, vBtnRestoreH HwndhBtnRestoreH Hidden1,                    %BtnRestoreHLocation%
    Gui, Example_with_tabs_:Add, Picture, vBtnRestoreP HwndhBtnRestoreP Hidden1,                    %BtnRestorePLocation%
    ; Gui, Example_with_tabs_:Add, Picture, vBtnMinN HwndhBtnMinN,                                    %BtnMinNLocation%
    Gui, Example_with_tabs_:Add, Picture, vBtnMinH HwndhBtnMinH Hidden1,                            %BtnMinHLocation%
    Gui, Example_with_tabs_:Add, Picture, vBtnMinP HwndhBtnMinP Hidden1,                            %BtnMinPLocation%
    ; Gui, Example_with_tabs_:Add, Picture, vBorderTop,                                               %BorderTopLocation%
    ; Gui, Example_with_tabs_:Add, Picture, vBorderBottom,                                            %BorderBottomLocation%
    ; Gui, Example_with_tabs_:Add, Picture, vBorderLeft,                                              %BorderLeftLocation%
    ; Gui, Example_with_tabs_:Add, Picture, vBorderRight,                                             %BorderRightLocation%

    MiniExample_with_tabs_CtlShowHide("Hide", "MenuHover")

    Gui, Example_with_tabs_:Color, 000000, FFFFFF
    xpos := xpos
    ypos := ypos
    Gui, Example_with_tabs_:Show, x470 y190 w980 h740, Helpy

    Gui, Example_with_tabs_:Font, s11, Segoe UI
    Gui, Example_with_tabs_:Add, Text, cWhite vMenuText1 HwndhMenuText1 BackgroundTrans 0x200 c9400D3, % "       Helpy"
    Gui, Example_with_tabs_:Add, Text, cWhite vMenuText2 HwndhMenuText2 BackgroundTrans 0x200, % "  Commonly used functions"
    Gui, Example_with_tabs_:Add, Text, cWhite vMenuText3 HwndhMenuText3 BackgroundTrans 0x200, % "  Self contained scripts"

    MiniExample_with_tabs_CtlShowHide("Hide", "MenuHover")

    Gui, Example_with_tabs_:Add, Tab2, vTab x0 y0 w0 h0 +Theme -Wrap AltSubmit Choose1, Tab1|Tab2|Tab3

    ; ------------------------------------------------------------------------------------------------------------------------------
    ; This is where tab 1 goes
    ; ------------------------------------------------------------------------------------------------------------------------------

    Gui, Tab, 1

    Gui, Example_with_tabs_:Font, s14, Segoe UI
    Gui, Example_with_tabs_:Add, Text, cWhite x1 y50  BackgroundTrans c9400D3, 
    Gui, Example_with_tabs_:Add, Text, vWhiteLineNumberMain1 x10 yp+2 w1280 0x10  ;Horizontal Line > Etched Gray
    Gui, Example_with_tabs_:Add, Text, cWhite xp+50 yp+6 w1 h20 , 
    
    #Include %A_ScriptDir%\AHKassociatedFiles\ScriptDividedByParts_version_for_direct_calling\TabPages\HelpyLauncherExample_with_tabs.ahk

    GuiControlGet, MenuText1PosInitial , Example_with_tabs_:Pos, MenuText1 
    GuiControlGet, MenuText2PosInitial , Example_with_tabs_:Pos, MenuText2
    GuiControlGet, MenuText3PosInitial , Example_with_tabs_:Pos, MenuText3 

    GuiControl, Example_with_tabs_:MoveDraw, MenuSelect, % "x" 10 " y" 1 " w" 80 " h" 4
    GuiControl, Example_with_tabs_:MoveDraw, MenuSelectPurple, % "x" 10 " y" 1 " w" 80 " h" 4

    GuiControlGet, MenuText1Pos, Example_with_tabs_:Pos, MenuText1  

    GuiControl,Example_with_tabs_:MoveDraw, MenuText1, % "x" 1 " y" 1 " w" MenuText1PosInitialW+30 " h" 48
    GuiControlGet, MenuText1Pos, Example_with_tabs_:Pos, MenuText1  
    GuiControl, Example_with_tabs_:MoveDraw, MenuText2, % "x" MenuText1PosX + MenuText1PosW " y" 1 " w" MenuText2PosInitialW+10 " h" 48
    GuiControlGet, MenuText2Pos, Example_with_tabs_:Pos, MenuText2  
    GuiControl, Example_with_tabs_:MoveDraw, MenuText3, % "x" MenuText2PosX + MenuText2PosW " y" 1 " w" MenuText3PosInitialW+10 " h" 48
    GuiControlGet, MenuText3Pos, Example_with_tabs_:Pos, MenuText3  

    MenuTextExperimentsSubTabs1PosX := MenuTextExperimentsSubTabs1PosX+MenuTextExperimentsSubTabs1PosW-1
    MenuTextExperimentsSubTabs1PosY := MenuTextExperimentsSubTabs1PosY
    GuiControl, Example_with_tabs_:MoveDraw, LineA, % "x" MenuTextExperimentsSubTabs1PosX " y" MenuTextExperimentsSubTabs1PosY

    MenuTextExperimentsSubTabs2PosX := MenuTextExperimentsSubTabs2PosX+MenuTextExperimentsSubTabs2PosW-1
    MenuTextExperimentsSubTabs2PosY := MenuTextExperimentsSubTabs2PosY
    GuiControl, Example_with_tabs_:MoveDraw, LineB, % "x" MenuTextExperimentsSubTabs2PosX " y" MenuTextExperimentsSubTabs2PosY

    MenuTextExperimentsSubTabs3PosX := MenuTextExperimentsSubTabs3PosX+MenuTextExperimentsSubTabs3PosW-1
    MenuTextExperimentsSubTabs3PosY := MenuTextExperimentsSubTabs3PosY
    GuiControl, Example_with_tabs_:MoveDraw, LineC, % "x" MenuTextExperimentsSubTabs3PosX " y" MenuTextExperimentsSubTabs3PosY

    MenuTextExperimentsSubTabs4PosX := MenuTextExperimentsSubTabs4PosX+MenuTextExperimentsSubTabs4PosW-1
    MenuTextExperimentsSubTabs4PosY := MenuTextExperimentsSubTabs4PosY
    GuiControl, Example_with_tabs_:MoveDraw, LineD, % "x" MenuTextExperimentsSubTabs4PosX " y" MenuTextExperimentsSubTabs4PosY

    ; SplashTextOff

    ; SplashTextOff
    ; RemoveToolTipClipboard_help2()

    ; OnMessage(0x0200, "WM_MOUSEMOVE")

    ; WinWaitNotActive, ahk_id %current_ID%
    ; previous_ID := current_ID
    ; GroupAdd, MyGui, % "ahk_id " . WinExist()
    ; OnMessage(0x114, "OnScroll") ; WM_HSCROLL

    Gui, Example_with_tabs_:+LastFound
    WinSet, Redraw 

    ; Example_with_tabs_CtlShowHide("Hide", "MenuHover")

return

GuiClose: 
MiniExitSub:
    ExitApp
    PID := GetScriptPID_Mini(ScriptName)
    WinClose, PID Cyber_Helpy.ahk
return

GetScriptPID_Mini(ScriptName) {
   DHW := A_DetectHiddenWindows
   TMM := A_TitleMatchMode
   DetectHiddenWindows, On
   SetTitleMatchMode, 2
   WinGet, PID, PID, \%ScriptName% - ahk_class AutoHotkey
   DetectHiddenWindows, %DHW%
   SetTitleMatchMode, %TMM%
   Return PID
}

MiniExample_with_tabs_GuiSize(GuiHwnd, EventInfo, Width, Height) {

    GuiControl, Example_with_tabs_:MoveDraw, Gui3CommonlyUsedFunctionsBackground, % "x" 1 " y" 1 " w" (Width - 1) - 138 " h" 31
    GuiControl, Example_with_tabs_:MoveDraw, Gui2CommonlyUsedFunctionsBackground, % "x" 1 " y" 34 " w" 226 " h" (Height - 4) - 31
    GuiControl, Example_with_tabs_:MoveDraw, MenuHover, % "x" 1 " y" 1 " w" Width " h" 48
    GuiControl, Example_with_tabs_:MoveDraw, MenuPressed, % "x" 1 " y" 1 " w" Width " h" 48
    GuiControl, Example_with_tabs_:MoveDraw, WhiteLineNumber2, % "x" 10 " y" 108 " w" Width - 18 " h" 2 
    GuiControl, Example_with_tabs_:MoveDraw, WhiteLineNumberMain1, % "x" 10 " y" 52 " w" Width - 18 " h" 2 
    GuiControl, Example_with_tabs_:MoveDraw, WhiteLineNumberMain2, % "x" 10 " y" 52 " w" Width - 18 " h" 2 
    GuiControl, Example_with_tabs_:MoveDraw, WhiteLineNumberMain3, % "x" 10 " y" 52 " w" Width - 18 " h" 2 
    GuiControl, Example_with_tabs_:MoveDraw, WhiteLineNumberMain4, % "x" 10 " y" 52 " w" Width - 18 " h" 2 
    GuiControl, Example_with_tabs_:MoveDraw, WhiteLineNumberMain5, % "x" 10 " y" 52 " w" Width - 18 " h" 2 
    GuiControl, Example_with_tabs_:MoveDraw, WhiteLineNumberMain6, % "x" 10 " y" 52 " w" Width - 18 " h" 2 
    GuiControl, Example_with_tabs_:MoveDraw, WhiteLineNumberMain7, % "x" 10 " y" 52 " w" Width - 18 " h" 2 
    GuiControl, Example_with_tabs_:MoveDraw, WhiteLineNumberMain8, % "x" 10 " y" 52 " w" Width - 18 " h" 2 
    GuiControl, Example_with_tabs_:MoveDraw, WhiteLineNumberMain, % "x" 10 " y" 32 " w" Width - 18 " h" 2 
    GuiControl, Example_with_tabs_:MoveDraw, WhiteLineNumberLesser1, % "x" 230 " y" 120 " w" Width - 236 " h" 2 
    GuiControl, Example_with_tabs_:MoveDraw, WhiteLineNumberLesser2, % "x" 230 " y" 260 " w" Width - 236 " h" 2 

    MiniExample_with_tabs_CtlShowHide("Hide", "MenuHover")

}

MiniExample_with_tabs_GuiRestore() {
    WinRestore
    MiniExample_with_tabs_CtlShowHide("Hide", "BtnRestoreN,BtnRestoreH,BtnRestoreP")
    MiniExample_with_tabs_CtlShowHide("Show", "BtnMaxN")

}

MiniExample_with_tabs_GuiMaximize() {
    WinMaximize
    MiniExample_with_tabs_CtlShowHide("Hide", "BtnMaxN,BtnMaxH,BtnMaxP")
    MiniExample_with_tabs_CtlShowHide("Show", "BtnRestoreN")
}

MiniExample_with_tabs_CtlShowHide(ShowHide, Controls*) {
    Static K, V

    For K, V In Controls {
        Split := StrSplit(V, ",")

        Loop % Split.MaxIndex() {
            GuiControl, % ShowHide, % Split[A_Index]
        }
    }
}

MiniExample_with_tabs_WWM_NCCALCSIZE(wParam, lParam, Msg, Hwnd) {
    return (A_Gui ? 0 : "")
}

MiniExample_with_tabs_WWM_NCACTIVATE(wParam, lParam, Msg, Hwnd) {
    return (A_Gui ? 1 : "")
}

MiniExample_with_tabs_WWM_NCHITTEST(wParam, lParam, Msg, Hwnd) {
    Global X, Y, gX, gY

	WinGetPos, gX, gY, gW, gH
	X := lParam << 48 >> 48, Y := lParam << 32 >> 48
	HL := X < gX + 6, HR := X >= (gX + gW) - 6
	HT := Y < gY + 6, HB := Y >= (gY + gH) - 6
    IfEqual, HT, 1, return "0x" (HL ? "D" : HR ? "E" : "C")
    IfEqual, HB, 1, return "0x" (HL ? "10" : HR ? "11" : "F")
    IfEqual
, HL, 1, return 0xA
    IfEqual, HR, 1, return 0xB
}

MiniExample_with_tabs_WWM_LBUTTONDOWN(wParam, lParam, Msg, Hwnd) {
    Global ; Assume-global mode
    Static Init := OnMessage(0x0201, "MiniExample_with_tabs_WWM_LBUTTONDOWN")

    If (MouseCtrl ~= hBtnBackP "|" hBtnMinP "|" hBtnMaxP "|" hBtnRestoreP "|" hBtnCloseP) {
        return
    }

    If (MouseCtrl ~= hMenuText1 "|" hMenuText2 "|" hMenuText3) {
        MiniExample_with_tabs_CtlShowHide("Show", "MenuPressed")
    }

    GuiControl, % (MouseCtrl = hBtnBackH ? "Show" : "Hide"), BtnBackP
    GuiControl, % (MouseCtrl = hBtnMinH ? "Show" : "Hide"), BtnMinP
    GuiControl, % (MouseCtrl = hBtnMaxH ? "Show" : "Hide"), BtnMaxP
    GuiControl, % (MouseCtrl = hBtnRestoreH ? "Show" : "Hide"), BtnRestoreP
    GuiControl, % (MouseCtrl = hBtnCloseH ? "Show" : "Hide"), BtnCloseP

    If (!MouseCtrl) {
        cX := X - gX, cY := Y - gY

        If (cY < 31) {
            PostMessage, 0xA1, 2
        }
    }
}

MiniExample_with_tabs_WM_MOUSEMOVE(wParam, lParam, Msg, Hwnd) {
    Global ; Assume-global mode
    Static Init := OnMessage(0x0200, "MiniExample_with_tabs_WM_MOUSEMOVE"), Hover := 0, Curr := ""

	DllCall("User32.dll\TrackMouseEvent", "UInt", &TME)
	MouseGetPos, MouseX, MouseY,, MouseCtrl, 2

    If (MouseCtrl ~= hBtnBackH "|" hBtnMinH "|" hBtnMaxH "|" hBtnRestoreH "|" hBtnCloseH) {
        return
    }

    If (MouseCtrl ~= hMenuText1 "|" hMenuText2 "|" hMenuText3 ) {
        MiniExample_with_tabs_CtlShowHide("Show", "MenuHover")
        IfEqual, Hover, 1, IfEqual, Curr, % MouseCtrl, return
        GuiControlGet, MenuPos, Example_with_tabs_:Pos, % MouseCtrl

        GuiControl, Example_with_tabs_:MoveDraw, MenuHover, % "x" MenuPosX " y" MenuPosY " w" MenuPosW " h" MenuPosH
        GuiControl, Example_with_tabs_:MoveDraw, MenuPressed, % "x" MenuPosX " y" MenuPosY " w" MenuPosW " h" MenuPosH

        GuiControl, Example_with_tabs_:MoveDraw, MenuHover, +BackgroundTrans

        Hover := 1, Curr := MouseCtrl
    }
    Else {
        MiniExample_with_tabs_CtlShowHide("Hide", "MenuHover"), Hover := 0
    } 

    GuiControl, % (MouseCtrl = hBtnBackN ? "Show" : "Hide"), BtnBackH
    GuiControl, % (MouseCtrl = hBtnMinN ? "Show" : "Hide"), BtnMinH
    GuiControl, % (MouseCtrl = hBtnMaxN ? "Show" : "Hide"), BtnMaxH
    GuiControl, % (MouseCtrl = hBtnRestoreN ? "Show" : "Hide"), BtnRestoreH
    GuiControl, % (MouseCtrl = hBtnCloseN ? "Show" : "Hide"), BtnCloseH
    IfEqual, MouseCtrl,, Try MiniExample_with_tabs_CtlShowHide("Hide", "BtnBackH,BtnBackP,BtnMinH,BtnMinP,BtnMaxH,BtnMaxP,BtnCloseH,BtnCloseP")

}

MiniExample_with_tabs_WM_LBUTTONUP(wParam, lParam, Msg, Hwnd) {
    Global ; Assume-global mode
    Static Init := OnMessage(0x0202, "MiniExample_with_tabs_WM_LBUTTONUP")

    DllCall("User32.dll\TrackMouseEvent", "UInt", &TME)
	MouseGetPos, MouseX, MouseY,, MouseCtrl, 2

    If (MouseCtrl ~= hMenuText1 "|" hMenuText2 "|" hMenuText3 ) {
        MiniExample_with_tabs_CtlShowHide("Show", "MenuHover")
        IfEqual, Hover, 1, IfEqual, Curr, % MouseCtrl, return
        ; GuiControlGet, MenuPos, Pos, % MouseCtrl
        GuiControlGet, MenuName, Example_with_tabs_:Name , % MouseCtrl

        ; ------------------------------------------------------------------------------------------------------------------------------
        ; This is where the rest of the tabs go
        ; ------------------------------------------------------------------------------------------------------------------------------

        if(MenuName=="MenuText2")
        {
            Gui, Tab, 2
            Gui, Example_with_tabs_:Font, s14, Segoe UI
            Gui, Example_with_tabs_:Add, Text, cWhite x1 y50 BackgroundTrans,

            if(var_to_check_if_WhiteLineNumberMain2_exists == 0){
                Gui, Example_with_tabs_:Add, Text, vWhiteLineNumberMain2 x10 yp+2 w1280 0x10  ;Horizontal Line > Etched Gray

                Gui, Example_with_tabs_:Add, Text, cWhite xp+50 yp+6 w1 h20 ,  
                settimer, MiniDalayedRunOfCommonlyUsedFunctions, -100

                var_to_check_if_WhiteLineNumberMain2_exists = 1
            }
        }
        if(MenuName=="MenuText3")
        {
            Gui, Tab, 3
            Gui, Example_with_tabs_:Font, s14, Segoe UI
            Gui, Example_with_tabs_:Add, Text, cWhite x1 y50 BackgroundTrans,

            if(var_to_check_if_WhiteLineNumberMain3_exists == 0){
                Gui, Example_with_tabs_:Add, Text, vWhiteLineNumberMain3 x10 yp+2 w1280 0x10  ;Horizontal Line > Etched Gray

                Gui, Example_with_tabs_:Add, Text, cWhite xp+50 yp+6 w1 h20 ,  
                settimer, MiniDalayedRunOfSelfContainedScripts, -100

                var_to_check_if_WhiteLineNumberMain3_exists = 1
            }
        }

        if(MenuPosX!=1)
        {
            GuiControl, Example_with_tabs_:MoveDraw, MenuSelect, % "x" MenuPosX + 10 " y" 1 " w" MenuPosW-24 " h" 4
        }

    }

    If (MouseCtrl ~= hSelfContainedScriptsSubTabs1) {

        hide_exe_whiteline_up()
        hide_all_exe_duplicate_file_names_gui_controls()

        hide_Ink_whiteline_up()
        hide_all_Ink_duplicate_file_names_gui_controls()

        show_ahk_whiteline_up()
        show_all_ahk_duplicate_file_names_gui_controls()

    }

    If (MouseCtrl ~= hSelfContainedScriptsSubTabs2) {

        hide_ahk_whiteline_up()
        hide_all_ahk_duplicate_file_names_gui_controls()

        hide_Ink_whiteline_up()
        hide_all_Ink_duplicate_file_names_gui_controls()

        show_exe_whiteline_up()
        show_all_exe_duplicate_file_names_gui_controls()

    }

    If (MouseCtrl ~= hSelfContainedScriptsSubTabs3) {

        hide_ahk_whiteline_up()
        hide_all_ahk_duplicate_file_names_gui_controls()

        hide_exe_whiteline_up()
        hide_all_exe_duplicate_file_names_gui_controls()

        show_Ink_whiteline_up()
        show_all_Ink_duplicate_file_names_gui_controls()

    }

	If (MouseCtrl = hBtnBackP) {
        MiniExample_with_tabs_CtlShowHide("Hide", "BtnBackN")
        GuiControl,, TitleText, 
        GuiControl, MoveDraw, TitleText, x14
    }

    If (MouseCtrl ~= hMenuText1 "|" hMenuText2 "|" hMenuText3 "|" hMenuText4 "|" hMenuText5 "|" hMenuText6 "|" hMenuText7 "|" hMenuText8) {
        GuiControlGet, MenuVar, Example_with_tabs_:Name, % MouseCtrl
        GuiControl, Example_with_tabs_:Choose, Tab, % SubStr(MenuVar, 9)
        indexNumForColour := 1

        Loop, 8 {
            
            if(A_Index!=1)
            {
                GuiControl, Example_with_tabs_:+cFFFFFF +Redraw, % "MenuText" A_Index
            }
            GuiControl, Example_with_tabs_:MoveDraw, % "MenuText" A_Index
            indexNumForColour++
        }

        if(MenuPosX==1)
        {
            GuiControl, Example_with_tabs_:+c9400D3 +Redraw, % MouseCtrl
            GuiControl, Example_with_tabs_:MoveDraw, % MouseCtrl
            GuiControl, Example_with_tabs_:Show, MenuSelectPurple
            GuiControl, Example_with_tabs_:Hide, MenuSelect

            GuiControl, MoveDraw, MenuSelectPurple, % " x" MenuPosX+12
        }
        if(MenuPosX!=1)
        {
             GuiControl, Example_with_tabs_:+c0078D7 +Redraw, % MouseCtrl
             GuiControl, Example_with_tabs_:MoveDraw, % MouseCtrl
             GuiControl, Example_with_tabs_:Show, MenuSelect
             GuiControl, Example_with_tabs_:Hide, MenuSelectPurple

             GuiControl, Example_with_tabs_:MoveDraw, MenuSelect, % " x" MenuPosX+12
        }
    }

    IfEqual, MouseCtrl, % hBtnMinP, WinMinimize
    IfEqual, MouseCtrl, % hBtnCloseP, GoSub, MiniExitSub
    IfEqual, MouseCtrl, % hBtnMaxP, Try MiniExample_with_tabs_GuiMaximize()
    IfEqual, MouseCtrl, % hBtnRestoreP, Try MiniExample_with_tabs_GuiRestore()
    MiniExample_with_tabs_CtlShowHide("Hide", "BtnBackP,BtnMinP,BtnMaxP,BtnRestoreP,BtnCloseP,MenuPressed")

}

MiniDalayedRunOfCommonlyUsedFunctions:
    #Include %A_ScriptDir%\AHKassociatedFiles\ScriptDividedByParts_version_for_direct_calling\TabPages\CommonlyUsedFunctionsExample_with_tabs.ahk
return

MiniDalayedRunOfSelfContainedScripts:
    #Include %A_ScriptDir%\AHKassociatedFiles\ScriptDividedByParts_version_for_direct_calling\TabPages\SelfContainedScriptsExample_with_tabs.ahk
return

MiniDalayedRunOfMenuText4:
    #Include %A_ScriptDir%\AHKassociatedFiles\ScriptDividedByParts_version_for_direct_calling\TabPages\TaggingExample_with_tabs.ahk
return

MiniDalayedRunOfMenuText5:
    #Include %A_ScriptDir%\AHKassociatedFiles\ScriptDividedByParts_version_for_direct_calling\TabPages\MouseConfigurationExample_with_tabs.ahk
return

MiniDalayedRunOfMenuText6:
    #Include %A_ScriptDir%\AHKassociatedFiles\ScriptDividedByParts_version_for_direct_calling\TabPages\Tscripts_version_for_direct_callingExample_with_tabs.ahk
return

MiniDalayedRunOfMenuText7:
    #Include %A_ScriptDir%\AHKassociatedFiles\ScriptDividedByParts_version_for_direct_calling\TabPages\MultilingualScripsExample_with_tabs.ahk
return

MiniDalayedRunOfMenuText8:
    #Include %A_ScriptDir%\AHKassociatedFiles\ScriptDividedByParts_version_for_direct_calling\TabPages\HelpySubTabsExample_with_tabs.ahk
return
